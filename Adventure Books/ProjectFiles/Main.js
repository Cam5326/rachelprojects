window.onload = main;

var loaders = 0;
var clickAllowed = true;
// create a global audio source
var audioListener = new THREE.AudioListener();
var track = new THREE.Audio( audioListener );

function main() {

   document.getElementById("skipButton").style.display = 'none';

   /* SCENE AND CAMERA */
   const scene = new THREE.Scene();
   const camera = new THREE.PerspectiveCamera(60,
    window.innerWidth / window.innerHeight, 0.1, 1000);

   camera.position.z = 5;
   
   /* STORY */
   var regRef = database.ref();
   var story, storyLoad = 0;
   var timeCompleted;

   regRef.on("value", function(data) {
      timeCompleted = data.val().timeCompleted;
      story = data.val()["story" + storyNum];
      storyLoad++;
   });

   /* DATE CHECK */
   var leggo = false

   /* AUDIO */
   // add listener to the camera
   camera.add( audioListener );

   // load a sound and set it as the Audio object's buffer
   var audioLoader = new THREE.AudioLoader();
    
   /* LIGHTS */
   const ambLgt = new THREE.AmbientLight(0xD0D0D0, 0.8);
   const dirLgt = new THREE.DirectionalLight(0xFFFFFF, 1);
   dirLgt.position.set(0, 0, 20);
   scene.add(dirLgt);
   scene.add(ambLgt);
   
   /* RENDERER */
   const renderer = new THREE.WebGLRenderer();
   renderer.setSize(window.innerWidth, window.innerHeight);
   renderer.autoClearColor = false;
   document.body.appendChild(renderer.domElement);

   var picLoader = new THREE.TextureLoader();
   
   /* CONTROLS */
   const controls = new THREE.OrbitControls(camera, renderer.domElement);
   controls.maxPolarAngle = 3 * Math.PI/4;
   controls.maxDistance = 1000;
   controls.minDistance = 0.1;
   controls.enabled = false;
   
   /* RAYCASTER */
   var raycaster = new THREE.Raycaster();
   var mouse = new THREE.Vector2();
   var isMouseDown = false, clickingOnOption = false, startClick = true;

   /* MOUSE STUFF */
   function onMouseMove( event ) {

      // calculate mouse position in normalized device coordinates
      // (-1 to +1) for both components

      mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
      mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
   }
   function onMouseDown( event ) {
      isMouseDown = true;
   }
   function onMouseUp( event ) {
      isMouseDown = false;
      startClick = true;
   }
   window.addEventListener( 'mousemove', onMouseMove, false );
   window.addEventListener( 'mousedown', onMouseDown, false );
   window.addEventListener( 'mouseup', onMouseUp, false );

   /* BUTTONS */
   document.getElementById("beginButton").addEventListener('click', () => {
      controls.enabled = true;
      document.getElementById("beginButton").style.display = 'none';

      if (leggo) {
         audioLoader.load( `../Resources/Tracks/Story${story.storyNum}/Track1.wav`, function( buffer ) {
            track.setBuffer( buffer );
            track.setLoop( false );
            track.setVolume( 0.5 );
            track.play();
            track.decision = story.decisionMap[0];
            track.first = true;
         });

         if (story.decisionMap[0]["hasPlayed"] == 1)
            document.getElementById("skipButton").style.display = 'block';

         track.onEnded = function() {
            document.getElementById("skipButton").style.display = 'none';

            var decisionRef = database.ref("story" + storyNum + "/decisionMap/0")
            decisionRef.update({
               "hasPlayed" : "1"
            });
            
            loaders++;
            track = new THREE.Audio( audioListener );
            audioLoader.load( `../Resources/Tracks/Story${story.storyNum}/Waiting.wav`, function( buffer ) {
               track.setBuffer( buffer );
               track.setLoop( true );
               track.setVolume( 0.5 );
               track.play();
            });
         }
      }
      else
      {
         loaders++;
         var timeLeft, time, unit;
         timeLeft = timeCompleted + 79200000 - Date.now();

         // At least two hours
         if (timeLeft > 7200000)
         {
            unit = "hours";
            time = Math.floor(timeLeft / 3600000);
         }
         // At least one hour
         else if (timeLeft > 3600000)
         {
            unit = "hour";
            time = 1;
         }
         // At least two minutes
         else if (timeLeft > 120000)
         {
            unit = "minutes";
            time = Math.floor(timeLeft / 60000);
         }
         // At least one minute
         else if (timeLeft > 60000)
         {
            unit = "minute";
            time = 1;
         }
         // At least two seconds
         else if (timeLeft > 2000)
         {
            unit = "seconds";
            time = Math.floor(timeLeft / 1000);
         }
         // One second or less
         else 
         {
            unit = "second";
            time = 1;
         }
         optText(scene, `You have ${time} ${unit} left`, font, txtMat, 0, 0, 0.4);
      }
   });

   document.getElementById("skipButton").addEventListener('click', () => {
      track.stop();
      document.getElementById("skipButton").style.display = 'none';

      if (track.first) {
         var decisionRef = database.ref("story" + storyNum + "/decisionMap/0")
         decisionRef.update({
            "hasPlayed" : "1"
         });
         
         loaders++;
         track = new THREE.Audio( audioListener );
         audioLoader.load( `../Resources/Tracks/Story${story.storyNum}/Waiting.wav`, function( buffer ) {
            track.setBuffer( buffer );
            track.setLoop( true );
            track.setVolume( 0.5 );
            track.play();
         });
      }
      else
         endTrack(track.decision, audioLoader, story, scene, picLoader, font, txtMat, storyNum);
   });

   /* TEXT */
   var fontLdr = new THREE.FontLoader();
   var font, skyBox, textMat;

   /* ANIMATE */
   function animate() {
      requestAnimationFrame( animate );
      
      //once the story has loaded the first time only
      if (storyLoad == 1)
      {         
         // If it's been 22 hours
         if (story && timeCompleted + 79200000 < Date.now())
            leggo = true;

         storyLoad++;
         skyBox = makeSkyBox(story);
         txtMat = new THREE.MeshBasicMaterial( { color: story.fontClr } );

         fontLdr.load( `../Resources/Fonts/${story.font}`, function ( loadedFont ) {
            font = loadedFont;
            
            if (leggo) {
               optText(scene, "Which will you choose?", font, txtMat, 0, 1.5, 0.5);
               optText(scene, story.decisionMap[1].optionTxt, font, txtMat, -2, -2, 0.25);
               optText(scene, story.decisionMap[2].optionTxt, font, txtMat, 2, -2, 0.25);
            }
            loaders++;
         });

         if (leggo) {
            loadNewPic(scene, picLoader, story.storyNum + "/" + story.decisionMap[1].optionPic + ".jpg", story.decisionMap[1].picBackClr,
             -2, story.decisionMap[1]);
            loadNewPic(scene, picLoader, story.storyNum + "/" + story.decisionMap[2].optionPic + ".jpg", story.decisionMap[2].picBackClr,
             2, story.decisionMap[2]);
         }
      }

      // update the picking ray with the camera and mouse position
      raycaster.setFromCamera( mouse, camera );

      // calculate objects intersecting the picking ray
      var intersects = raycaster.intersectObjects( scene.children );
      var curOption, mouseOverBox = false;

      // Option Clicked
      for ( var i = 0; i < intersects.length; i++ ) {
         if (isMouseDown && startClick && clickAllowed && intersects[i].object.geometry.type == "BoxBufferGeometry")
            clickingOnOption = true;

         if (intersects[i].object.geometry.type == "BoxBufferGeometry") {
            mouseOverBox = true;  
            curOption = intersects[i].object;
         }
      }
      // Won't be valid if mouse on something else
      if (clickAllowed && !mouseOverBox)
         clickingOnOption = false;       

      if (isMouseDown)
         startClick = false;

      // Only activate once finished clicking
      if (clickAllowed && !isMouseDown && clickingOnOption && curOption.material[5].color.r == 1)
      {
         clickAllowed = false;
         isMouseDown = false, clickingOnOption = false, startClick = true;

         track.stop();
         story.decisionMap.forEach(decision => {
            var imageSrc = curOption.material[5].map.image.src;
            if (decision.optionPic + ".jpg" === imageSrc.substr(imageSrc.indexOf(`Story${story.storyNum}`) + 7))
            {
               if (decision.hasPlayed == 1)
               {
                  if (decision.altEnding)
                  {
                     decision.id = decision.altEnding;
                     decision.altEnding = "";
                  }
                  else
                     document.getElementById("skipButton").style.display = 'block';
               }

               destroyOldDecisions(scene);
               track = new THREE.Audio( audioListener );
               audioLoader.load( `../Resources/Tracks/Story${story.storyNum}/Track${decision.id}.wav`, function( buffer ) {
                  track.setBuffer( buffer );
                  track.setLoop( false );
                  track.setVolume( 0.5 );
                  track.play();
                  track.decision = decision;
               });
               track.onEnded = function() {
                  endTrack(decision, audioLoader, story, scene, picLoader, font, txtMat, storyNum);
               };
            }
         });
      }

      if (loaders >= 2) {
         scene.children.forEach(child => {
            if (child.geometry && child.geometry.type == "BoxBufferGeometry")
               child.rotation.y += 0.01;
         });
         skyBox.mesh.position.copy(camera.position);
         renderer.render( skyBox.scene, camera );
         renderer.render( scene, camera );
      }
   }
   animate();
}

function makeSkyBox(story) {
   const skyBox = new THREE.Scene();
   const loader = new THREE.TextureLoader();
   const texture = loader.load(
      `../Resources/Photos/SkyImages/${story.backPhoto}`,
   );
   texture.magFilter = THREE.LinearFilter;
   texture.minFilter = THREE.LinearFilter;
   
   const shader = THREE.ShaderLib.equirect;
   const material = new THREE.ShaderMaterial({
      fragmentShader: shader.fragmentShader,
      vertexShader: shader.vertexShader,
      uniforms: shader.uniforms,
      depthWrite: false,
      side: THREE.BackSide,
   });
   material.uniforms.tEquirect.value = texture;
   const plane = new THREE.BoxBufferGeometry(2, 2, 2);
   var skyMesh = new THREE.Mesh(plane, material);
   skyBox.add(skyMesh);
   
   return {scene: skyBox, mesh: skyMesh};
}
function destroyOldDecisions(scene)
{
   var removeChildren = [];
   scene.children.forEach(child => {
      if (child.geometry && (child.geometry.type == "BoxBufferGeometry" || child.geometry.type == "TextGeometry"))
         removeChildren.push(child);
   });
   if (removeChildren.length > 0)
   {
      removeChildren.forEach(child => {
         scene.children.splice( scene.children.indexOf(child), 1 );
      });
   }
}
function loadNewPic(scene, picLoader, picName, backColor, xLoc, decision)
{
   var hasExplored;
   hasExplored = decision["fullyExplored"];
   var fullClr = backColor;
   if (hasExplored == "1")
   {
      var red = Math.floor(Number("0x" + backColor.substr(1, 2)) * 0.27).toString(16);
      var green = Math.floor(Number("0x" + backColor.substr(3, 2)) * 0.27).toString(16);
      var blue = Math.floor(Number("0x" + backColor.substr(5, 2)) * 0.27).toString(16);
      if (red.length == 1)
         red = "0" + red;
      if (green.length == 1)
         green = "0" + green;
      if (blue.length == 1)
         blue = "0" + blue;

      fullClr = "#" + red + green + blue;
   }

   var backMat = new THREE.MeshBasicMaterial( { color: `${fullClr}` } );
   var optGmy = new THREE.BoxBufferGeometry(2, 2, 0.1);

   picLoader.load( `../Resources/Photos/Story${picName}`,
      function ( texture ) {
         var picClr = '#FFFFFF';

         if (hasExplored == "1")
            picClr = '#444444';

         // in this example we create the material when the texture is loaded
         var optionPic = new THREE.MeshBasicMaterial ( { map: texture, color: picClr} );
         var optMats = [backMat, backMat, backMat, backMat, optionPic, optionPic];

         var option = new THREE.Mesh(optGmy, optMats);

         option.position.x = xLoc;
         
         scene.add(option);

         clickAllowed = true;
      },
      undefined,

      function (err)
      {
         console.log('an error occured');
      }
   );
}

function optText(scene, text, font, txtMat, xLoc, yLoc, size)
{
   var optTxtGmy = new THREE.TextGeometry( text, {
      font: font,
      size: size,
      height: 0.05,
      curveSegments: 20
   } );
   optTxtGmy.center(optTxtGmy);
   var optTxt = new THREE.Mesh(optTxtGmy, txtMat);

   optTxt.position.x = xLoc;
   optTxt.position.y = yLoc;

   scene.add(optTxt);
}

function endTrack(decision, audioLoader, story, scene, picLoader, font, txtMat, storyNum)
{
   document.getElementById("skipButton").style.display = 'none';

   var decisionRef = database.ref(`story${storyNum}/decisionMap/${story.decisionMap.indexOf(decision)}`);
   // Make sure we log that track has played
   
   decisionRef.update({
      "hasPlayed" : "1"
   });

   //If it's not an ending
   if (decision.option1Id)
   {
      track = new THREE.Audio( audioListener );
      audioLoader.load( `../Resources/Tracks/Story${story.storyNum}/Waiting.wav`, function( buffer ) {
         track.setBuffer( buffer );
         track.setLoop( true );
         track.setVolume( 0.5 );
         track.play();
      });

      if (decision.option3Id)
         optText(scene, "Which will you choose?", font, txtMat, 2, 1.5, 0.5);
      else
         optText(scene, "Which will you choose?", font, txtMat, 0, 1.5, 0.5);

      var option = story.decisionMap[(parseInt(decision.option1Id) - 1)];
      
      // Check if option is fully explored
      if (option.fullyExplored == "0") {
         if ((!option.option1Id || story.decisionMap[(parseInt(option.option1Id) - 1)].fullyExplored == "1") &&
             (!option.option2Id || story.decisionMap[(parseInt(option.option2Id) - 1)].fullyExplored == "1") &&
             (!option.option3Id || story.decisionMap[(parseInt(option.option3Id) - 1)].fullyExplored == "1") &&
             option.hasPlayed == "1" && !option.altEnding) {
            var optRef = database.ref(`story${storyNum}/decisionMap/${story.decisionMap.indexOf(option)}`);
            optRef.update({
               "fullyExplored" : "1"
            }).then(function(){
               option.fullyExplored = "1";
               firstOption(decision, story, scene, picLoader, font, txtMat, storyNum, option);
            });
         }
         else
            firstOption(decision, story, scene, picLoader, font, txtMat, storyNum, option);
      }
      else
         firstOption(decision, story, scene, picLoader, font, txtMat, storyNum, option);
   }
   else if (decision.ending && decision.ending > 0)
   {
      if (!decision.altEnding) {
         decisionRef.update({
            "fullyExplored" : "1"
         });
      }

      var regRef = database.ref();
      var storyRef = database.ref(`story${storyNum}/`);

      regRef.update({
         "timeCompleted" : Date.now()
      });

      storyRef.update({
         "endingsLeft" : `${story.endingsLeft - 1}`
      });

      var endingFile;
      optText(scene, "The End", font, txtMat, 0, 0, 0.75);

      switch(parseInt(decision.ending)) {
         case 1: endingFile = "BadEnding"; break;
         case 2: endingFile = "NeutralEnding"; break;
         case 3: endingFile = "GoodEnding"; break;
         default: break;
      }
      track = new THREE.Audio( audioListener );
      audioLoader.load( `../Resources/Tracks/Story${story.storyNum}/${endingFile}.wav`, function( buffer ) {
         track.setBuffer( buffer );
         track.setLoop( false );
         track.setVolume( 0.5 );
         track.play();
      });
      track.onEnded = function() {
         if (parseInt(story.endingsLeft) == 0)
         {
            track = new THREE.Audio( audioListener );
            audioLoader.load( `../Resources/Tracks/Story${story.storyNum}/TheEnd.wav`, function( buffer ) {
               track.setBuffer( buffer );
               track.setLoop( false );
               track.setVolume( 0.5 );
               track.play();
            });
         }
      };
   }
}

function firstOption(decision, story, scene, picLoader, font, txtMat, storyNum, option) {
   if (decision.option2Id) {
      // Add left pic
      loadNewPic(scene, picLoader, story.storyNum + "/" + option.optionPic + ".jpg", option.picBackClr, -2, option);
      optText(scene, option.optionTxt, font, txtMat, -2, -2, 0.25);

      option = story.decisionMap[parseInt(decision.option2Id) - 1];

      // Check if option is fully explored
      if (option.fullyExplored == "0") {
         if ((!option.option1Id || story.decisionMap[(parseInt(option.option1Id) - 1)].fullyExplored == "1") &&
            (!option.option2Id || story.decisionMap[(parseInt(option.option2Id) - 1)].fullyExplored == "1") &&
            (!option.option3Id || story.decisionMap[(parseInt(option.option3Id) - 1)].fullyExplored == "1") &&
            option.hasPlayed == "1" && !option.altEnding) {
            var optRef = database.ref(`story${storyNum}/decisionMap/${story.decisionMap.indexOf(option)}`);
            optRef.update({
               "fullyExplored" : "1"
            }).then(function(){
               option.fullyExplored = "1";
               secondOption(decision, story, scene, picLoader, font, txtMat, storyNum, option);
            });
         }
         else
            secondOption(decision, story, scene, picLoader, font, txtMat, storyNum, option);
      }
      else
         secondOption(decision, story, scene, picLoader, font, txtMat, storyNum, option);
   }
   //Next one doesn't have options
   else
   {
      loadNewPic(scene, picLoader, story.storyNum + "/" + option.optionPic + ".jpg", option.picBackClr, 0, option);
      optText(scene, option.optionTxt, font, txtMat, 0, -2, 0.25);
   }
}

function secondOption(decision, story, scene, picLoader, font, txtMat, storyNum, option) {
   //Add right pic
   loadNewPic(scene, picLoader, story.storyNum + "/" + option.optionPic + ".jpg", option.picBackClr, 2, option);
   optText(scene, option.optionTxt, font, txtMat, 2, -2, 0.25);
   
   //Weird 3rd option
   if (decision.option3Id)
   {
      option = story.decisionMap[parseInt(decision.option3Id) - 1];

      // Check if option is fully explored
      if (option.fullyExplored == "0") {
         if ((!option.option1Id || story.decisionMap[(parseInt(option.option1Id) - 1)].fullyExplored == "1") &&
            (!option.option2Id || story.decisionMap[(parseInt(option.option2Id) - 1)].fullyExplored == "1") &&
            (!option.option3Id || story.decisionMap[(parseInt(option.option3Id) - 1)].fullyExplored == "1") &&
            option.hasPlayed == "1" && !option.altEnding) {
            var optRef = database.ref(`story${storyNum}/decisionMap/${story.decisionMap.indexOf(option)}`);
            optRef.update({
               "fullyExplored" : "1"
            }).then(function(){
               option.fullyExplored = "1";
               thirdOption(story, scene, picLoader, font, txtMat, storyNum, option);
            });
         }
         else
            thirdOption(story, scene, picLoader, font, txtMat, storyNum, option);
      }
      else
         thirdOption(story, scene, picLoader, font, txtMat, storyNum, option);
   }
}

function thirdOption(story, scene, picLoader, font, txtMat, storyNum, option) {
   loadNewPic(scene, picLoader, story.storyNum + "/" + option.optionPic + ".jpg", option.picBackClr, 6, option);
   optText(scene, option.optionTxt, font, txtMat, 6, -2, 0.25);
}