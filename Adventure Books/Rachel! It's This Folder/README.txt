Hey Rachel!

Happy Birthday!!

For your present, you're gonna be able to hear me read you a story every day till we get married! Woo!

Here's how this works...

1. Launch which story you want to hear
2. Listen to the audio
3. Make a decision by clicking on an option box
4. Repeat steps 2 and 3 until you get to an ending
5. Wait till tomorrow for your next story!
6. Once you've completed a book, there's these trivia things in the back of the books that you may enjoy. Call me to do so.

Some rules...

1. DO NOT mess with the code or open the Resources folder. ONLY the "Rachel! It's this Folder" folder

And yeah, that's the only rule. I had others, but I fixed them.

Try to answer these as if you were actually in the situation deciding, and only make decisions you would never choose if you have
already chosen the other one. Ya know? This was a ton of fun to make, so I really hope you enjoy it.

Just some notes about what you can expect...

For the decision making, you will be allowed to skip any audio track that you've heard already. This is because I figured after 100
or so times you may want to skip the first couple tracks that you've heard so many times. After you've gone through them all once
and after we're married, I will remove all timers and restrictions so you can go through them as much as you'd like.

If you have fully explored an option, meaning all possible endings have been reached, that option will be greyed out and you
won't be able to click it. So you won't have to remember where you've been! Although if you find it fun, you can keep track 
of all the decisions you've made, I know I would.

So after you reach an ending, you will have 22 hours till you can listen again. If you launch any story, it will
tell you how much time you have left. I made it this way so that if you wanted to listen every night before bed, you could have
some flexibility depending on when you go to be each night. I can adjust this number if you need though.

While recording the audio to these, I made some very funny mistakes, and at least for the first story, I fully intended
on cutting them out, and I did edit out most of them, but some were just too great and I knew you'd love them. So prepare
to hear some flubbing and burps throughout the story.

When searching for the option photos, there were some crazy ones that came up that had nothing
to do with what I had searched, so I thought it'd be fun to make each story have one and only one photo that has absolutely
nothing to do with the actual decision you're making. So it'll be fun for you to look for em.

I didn't re-record any mistakes I made in the recording studio, so there are just a couple story specific mistakes...
Also, it's important to note that I recorded the first story in the studio at school, but since ya know... pandemic...
I had to do the rest at home. That's why there's a quality difference.

Abominable Snowman
1. I mispronounced "Khaki" somewhere and didn't know I got it wrong until I was editing :D
2. I said "kind of offer" instead of "kind offer"
3. I mispronounce "torrential" (facepalm)
4. I said "mentioned" instead of "motioned"
5. "icy chest" instead of "icy crest" :D

Journey Under the Sea
1. I say "lockout" instead of "lookout"
2. I accidentally said "honor" instead of "horror"

Space and Beyond
1. You can definitely hear the fan in the background in a few tracks
2. I said "luggage" rather than "language" lol
3. I said "significantly" instead of "specifically"
4. I said "additional" instead of "artificial"
5. I said "protect" instead of "prevent"
